﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using static WearAppDemo.APIManager;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WearAppDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
        private async void Page_Loaded(Object sender, RoutedEventArgs e)
        {
            try
            {
                var postion = await LocationData.getPosition();
                var lat = postion.Coordinate.Latitude;
                var lon = postion.Coordinate.Longitude;

                RootObject myWearther = await APIManager.Getweather(lat, lon);
                string icon = string.Format("ms-appx:///Assets/Weather/{0}.png", myWearther.weather[0].icon);

                ResultImage.Source = new BitmapImage(new Uri(icon, UriKind.Absolute));
                TempTextBlock.Text = ((double)myWearther.main.temp).ToString();
                DescriptTextBlock.Text = myWearther.weather[0].description;
                locationTextBlock.Text = myWearther.name;
            }
            catch(Exception ex)
            {
                locationTextBlock.Text = "Không lấy được tọa độ ";
            }
        }
    }
}
